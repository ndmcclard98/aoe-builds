import React, { useState, useEffect } from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import { db } from "../../utils/firebaseConfig";
import { onValue, ref } from "firebase/database";
import "./CivilizationCards.css";

export default function CivilizationCards(searchQuery) {
  //Create an array to push civs into from firebase
  const [currentCiv, setCurrentCiv] = useState([]);

  useEffect(() => {
    return () => {
      //data is the civilizations path from {db} from firebase
      const data = ref(db, "civilizations");

      //onValue generates an array of all data points from the data variable
      onValue(data, (snapshot) => {
        snapshot.val().map((e) => {
          setCurrentCiv((currentCiv) => [...currentCiv, e]);
        });
      });
    };
  }, []);

  //Sorts the civilization object by a given property
  function civSorter(property) {
    var sortOrder = 1;
    if (property[0] === "-") {
      sortOrder = -1;
      property = property.substr(1);
    }
    return function (a, b) {
      var result =
        a[property] < b[property] ? -1 : a[property] > b[property] ? 1 : 0;
      return result * sortOrder;
    };
  }

  const filteredCivs = currentCiv.filter((curData) => {
    const isMatch = curData.name
      .toLowerCase()
      .includes(searchQuery.searchQuery.toLowerCase());
    console.log(curData.name, "matches:", isMatch);
    return isMatch;
  });

  //Sort civilizations by the name key using civSorter function
  currentCiv.sort(civSorter("name"));

  //Map through the civilizations from firebase and render a card for each civ
  return (
    <div className="row-of-cards">
      {filteredCivs.map((curData) => {
        return (
          <div className="flip-card">
            <div className={"flip-card-inner"}>
              <Card className="flip-card-front">
                {console.log(curData.img)}
                <CardMedia component="img" image={curData.img} alt="Front" />
                <div className="modern-text-bg">
                  <Typography
                    variant="body1"
                    color="text.secondary"
                    id={"civ-text"}
                  >
                    {curData.name.toUpperCase()}
                  </Typography>
                </div>
              </Card>
              <Paper className="flip-card-back">
                <Typography
                  variant="body1"
                  color="text.secondary"
                  id={"civ-text"}
                >
                  {curData.civilization_bonus}
                </Typography>
              </Paper>
            </div>
          </div>
        );
      })}
    </div>
  );
}
