import React, { useState, useEffect } from "react";
import { db } from "../utils/firebaseConfig";
import { onValue, ref } from "firebase/database";
import "./BuildOrdersPage.css";

const BuildOrdersPage = () => {
  const BuildOrderSteps = [
    {
      currentStep: "6 on sheep",
      wood: 0,
      food: 6,
      gold: 0,
      stone: 0,
    },
    {
      currentStep: "3 on wood",
      wood: 3,
      food: 6,
      gold: 0,
      stone: 0,
    },
    {
      currentStep: "1 lure boar, then +1 on sheep",
      wood: 3,
      food: 8,
      gold: 0,
      stone: 0,
    },
    {
      currentStep: "4 on berries",
      wood: 3,
      food: 12,
      gold: 0,
      stone: 0,
    },
    {
      currentStep: "+2 on boar, move 1 to farm",
      wood: 3,
      food: 14,
      gold: 0,
      stone: 0,
    },
    {
      currentStep: "Rest to wood",
      wood: 5,
      food: 14,
      gold: 0,
      stone: 0,
    },
  ];
  const [buildOrders, setBuildOrders] = useState([]);

  useEffect(() => {
    // Reference to the "build_orders" node in firebase
    const buildOrdersRef = ref(db, "build_orders");

    // Fetch the data from the "build_orders" node
    onValue(buildOrdersRef, (snapshot) => {
      const buildOrdersData = snapshot.val();
      console.log(buildOrdersData["17_pop_khmer_scouts"]);
    });
  }, []);

  return (
    <div className="build-orders-page">
      <h1 className="page-title">Age of Empires 2 Build Orders</h1>
      <table className="build-order-table">
        <thead>
          <tr>
            <th>Step</th>
            <th>Wood</th>
            <th>Food</th>
            <th>Gold</th>
            <th>Stone</th>
          </tr>
        </thead>
        <tbody>
          {BuildOrderSteps.map((step, index) => (
            <tr key={index}>
              <td>{step.currentStep}</td>
              <td>{step.wood}</td>
              <td>{step.food}</td>
              <td>{step.gold}</td>
              <td>{step.stone}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default BuildOrdersPage;
