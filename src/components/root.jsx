import React, { useState, useEffect } from "react";
import { Outlet, NavLink } from "react-router-dom";
import { onAuthStateChanged } from "firebase/auth";
import { signOut } from "firebase/auth";
import { auth } from "../utils/firebaseConfig";

export default function Root() {
  const [curUser, setcurUser] = useState("");

  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        // User is signed in, see docs for a list of available properties
        // https://firebase.google.com/docs/reference/js/firebase.User
        const uid = user.uid;
        setcurUser(user);
        console.log(user);
        // ...
        console.log("uid", uid);
      } else {
        // User is signed out
        // ...
        console.log("user is logged out");
      }
    });
  }, [curUser]);

  const handleLogout = () => {
    signOut(auth)
      .then(() => {
        // Sign-out successful.
        setcurUser(null); // I don't think we need this
        console.log("Signed out successfully");
      })
      .catch((error) => {
        // An error happened.
      });
  };
  return (
    <>
      <div className="navbar">
        <div className="search-box">AOE Builds</div>
        <nav>
          <ul className="nav-links">
            <div className="menu">
              <li>
                <NavLink
                  to={`build-orders/1`}
                  className={({ isActive, isPending }) =>
                    isActive ? "active" : isPending ? "pending" : ""
                  }
                >
                  Build Orders
                </NavLink>
              </li>
              <li>
                <NavLink to={`civ-tech-tree/2`}>Civilizations</NavLink>
              </li>
              {curUser ? (
                <li>
                  <button onClick={handleLogout}>Logout</button>
                </li>
              ) : (
                <li>
                  <NavLink to={`login`}>
                    {curUser ? curUser.email : "Login"}
                  </NavLink>
                </li>
              )}
            </div>
          </ul>
        </nav>
      </div>
      <div id="detail">
        <Outlet />
      </div>
    </>
  );
}
