import React, { useState, useEffect } from "react";
import CivilizationCards from "../components/civilizationCards/CivilizationCards";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import "./CivilizationPage.css";

export default function CivilizationPage() {
  const [searchQuery, setSearchQuery] = useState("");

  const handleSearch = (event) => {
    setSearchQuery(event.target.value);
  };

  return (
    <div className="civ-page">
      {/* Civilization Cards */}
      <div className="row">
        <CivilizationCards searchQuery={searchQuery} />
      </div>

      {/* Search Bar */}
      <div className="search-bar-container">
        <TextField
          label="Search Civilization"
          value={searchQuery}
          onChange={handleSearch}
          variant="outlined"
        />
        <div class="nk-widget nk-widget-highlighted">
          <h4 class="nk-widget-title">
            <span>
              <span class="text-main-1">Top 3</span> Recent Builds
            </span>
          </h4>
          <div class="nk-widget-content">
            <div class="nk-widget-post">
              <a href="blog-article.html" class="nk-post-image">
                <img
                  src="https://i.ytimg.com/vi/JsTNM7j6fs4/hqdefault.jpg?sqp=-oaymwEcCPYBEIoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLDGCOZxTP9prCa47VPq0whnzOjnTQ"
                  alt=""
                />
              </a>
              <h3 class="nk-post-title">
                <a href="blog-article.html">
                  The BEST Beginner Build Order In AoE2
                </a>
              </h3>
              <div class="nk-post-date">
                <span class="fa fa-calendar"></span> Hera
              </div>
            </div>

            <div class="nk-widget-post">
              <a href="blog-article.html" class="nk-post-image">
                <img
                  src="https://i.ytimg.com/vi/VxwHrzdOMmE/hqdefault.jpg?sqp=-oaymwEcCPYBEIoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLCuCO7vihFN3LgN1AS_vgp1s8Hh_g"
                  alt=""
                />
              </a>
              <h3 class="nk-post-title">
                <a href="blog-article.html">How To Do The PERFECT Dark Age</a>
              </h3>
              <div class="nk-post-date">
                <span class="fa fa-calendar"></span> Hera
              </div>
            </div>

            <div class="nk-widget-post">
              <a href="blog-article.html" class="nk-post-image">
                <img
                  src="https://i.ytimg.com/vi/P718AmycruQ/hqdefault.jpg?sqp=-oaymwEcCPYBEIoBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLCzY70oV4u8T559AVyWBcEconmBvA"
                  alt=""
                />
              </a>
              <h3 class="nk-post-title">
                <a href="blog-article.html">
                  THE FASTEST ARCHER RUSH BUILD ORDER YOU WILL EVER SEE
                </a>
              </h3>
              <div class="nk-post-date">
                <span class="fa fa-calendar"></span> Hera
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
