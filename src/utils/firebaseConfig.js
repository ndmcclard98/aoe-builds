import { initializeApp } from "firebase/app";
import { getDatabase, ref } from "firebase/database";
import { getAuth } from "firebase/auth";

// const firebaseConfig = {
//   apiKey: import.meta.env.VITE_FIREBASE_API_KEY,
//   authDomain: import.meta.env.VITE_FIREBASE_AUTH_DOMAIN,
//   databaseURL: import.meta.env.VITE_FIREBASE_DATABASE_URL,
//   projectId: import.meta.env.VITE_FIREBASE_PROJECT_ID,
//   storageBucket: import.meta.env.VITE_FIREBASE_STORAGE_BUCKET,
//   messagingSenderId: import.meta.env.VITE_FIREBASE_MESSAGE_SENDER_ID,
//   appId: import.meta.env.VITE_FIREBASE_APP_ID,
//   measurementId: import.meta.env.VITE_FIREBASE_MEASUREMENT_ID,
// };

const firebaseConfig = {
  apiKey: "AIzaSyA8cCfFLfvib7SEQ9TNDSO59FqO4KE07UM",
  authDomain: "aoe-builds.firebaseapp.com",
  databaseURL: "https://aoe-builds-default-rtdb.firebaseio.com",
  projectId: "aoe-builds",
  storageBucket: "aoe-builds.appspot.com",
  messagingSenderId: "659391366237",
  appId: "1:659391366237:web:4f1296fee772e1adefb9b4",
  measurementId: "G-XYBE3SL3SR",
};

const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const db = getDatabase(app);
