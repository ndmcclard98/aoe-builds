import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import "./index.css";
import Root from "./components/root";
import ErrorPage from "./error-page";
import BuildOrdersPage from "./components/BuildOrdersPage";
import Index from "./components/index";
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import CivilizationPage from "./pages/CivilizationPage";
import LoginPage from "./components/auth/LoginPage";
import SignupPage from "./components/auth/SignupPage";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    errorElement: <ErrorPage />,
    children: [
      { index: true, element: <Index /> },
      {
        path: "build-orders/:buildId",
        element: <BuildOrdersPage />,
      },
      {
        path: "civ-tech-tree/:buildId",
        element: <CivilizationPage />,
      },
      {
        path: "login",
        element: <LoginPage />,
      },
      {
        path: "signup",
        element: <SignupPage />,
      },
    ],
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
