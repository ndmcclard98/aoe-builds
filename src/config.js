require("dotenv").config();

const FIREBASE_API_KEY = process.env.FIREBASE_API_KEY;

module.exports = {
  FIREBASE_API_KEY,
};
